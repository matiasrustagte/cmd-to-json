const parseArgs = require('minimist');

const setProperty = function (obj, chain, value, maxDepth = 0) {
  const [head, ...tail] = chain;
  if (tail.length) {
    if (!obj[head])
      obj[head] = {};
    if (maxDepth)
      setProperty(obj[head], tail, value, maxDepth - 1);
  } else {
    obj[head] = value;
  }
};

module.exports = function (maxDepth = 4) {
  const { _, ...args } = parseArgs(process.argv.slice(2));
  for (let key in args) {
    if (!key.includes('-'))
      continue;
    setProperty(args, key.split('-'), args[key], maxDepth);
    delete args[key];
  }
  return args;
};